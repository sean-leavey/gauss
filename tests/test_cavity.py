import unittest

import numpy as np
from gauss import FabryPerot
from gauss.constants import c0

class FabryPerotTestCase(unittest.TestCase):
    """Fabry Perot cavity tests against Finesse results"""
    def setUp(self):
        self.length = 11.3673
        self.r1_p = 1 - 8220e-6
        self.r2_p = 1 - 16e-6
        self.rc1 = 5.7
        self.rc2 = 5.7
        self.cavity = FabryPerot(self.length, self.r1_p, self.r2_p, self.rc1, self.rc2)

    def test_g_factor(self):
        self.assertAlmostEqual(self.cavity.g_factor, 0.98856, places=5)

    def test_fsr(self):
        self.assertAlmostEqual(self.cavity.fsr, c0 / (2 * self.length))
    
    def test_fwhm(self):
        fsr = c0 / (2 * self.length)
        r1_a = np.sqrt(self.r1_p)
        r2_a = np.sqrt(self.r2_p)
        fwhm = 2 * fsr / np.pi * np.arcsin((1 - r1_a * r2_a) / (2 * np.sqrt(r1_a * r2_a)))
        self.assertAlmostEqual(self.cavity.fwhm, fwhm)

    def test_finesse(self):
        self.assertAlmostEqual(self.cavity.finesse, 759.758, places=5)
    
    def test_zr(self):
        self.assertAlmostEqual(self.cavity.zr, 304.84041e-3, places=5)

    def test_z1_z2(self):
        self.assertAlmostEqual(self.cavity.z1, -5.68365, places=5)
        self.assertAlmostEqual(self.cavity.z2, 5.68365, places=5)

    def test_w0(self):
        self.assertAlmostEqual(self.cavity.w0, 321e-6, places=5)

    def test_w1_w2(self):
        self.assertAlmostEqual(self.cavity.w1, 5.9994398e-3, places=5)
        self.assertAlmostEqual(self.cavity.w2, 5.9994398e-3, places=5)

    def test_w1_w2_alt(self):
        """Alternate method to calculate beam sizes"""
        w1 = self.cavity.w0 * np.sqrt(1 + (self.cavity.z1 / self.cavity.zr) ** 2)
        w2 = self.cavity.w0 * np.sqrt(1 + (self.cavity.z2 / self.cavity.zr) ** 2)
        self.assertAlmostEqual(self.cavity.w1, w1)
        self.assertAlmostEqual(self.cavity.w2, w2)
    
    def test_rt_gouy(self):
        self.assertAlmostEqual(self.cavity.gouyt, np.radians(-12.280383), places=5)
    
    def test_mode_separation(self):
        self.assertAlmostEqual(self.cavity.mode_separation, -449.82418e3, places=2)