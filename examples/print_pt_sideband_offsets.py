"""Calculate sideband frequency offsets from the higher order mode
resonances in the AEI 10 m prototype SQL arm cavities, considering
also slight variation in the radii of curvature of the mirrors."""

import numpy as np
from scipy.constants import golden_ratio
from tabulate import tabulate

from gauss.cavity import FabryPerot


if __name__ == "__main__":
    lcav = 11.3673
    itm_t_p = 8220e-6
    etm_t_p = 16e-6
    itm_roc = 5.7
    etm_roc = 5.7
    d_roc = 0.001

    maxhom = 20
    nroc = 11
    nresults = 20

    nom_cavity = FabryPerot(lcav, 1 - itm_t_p, 1 - etm_t_p, itm_roc, etm_roc)

    print("Nominal cavity parameters:")
    print(nom_cavity)
    print()

    fmod = 21.34e6

    prows = []
    nrows = []

    for roc_offset in np.linspace(-d_roc / 2, d_roc / 2, nroc):
        cavity = FabryPerot(lcav, 1 - itm_t_p, 1 - etm_t_p, itm_roc + roc_offset, etm_roc)

        for lm in range(maxhom + 1): # Higher order mode
            pres = cavity.resonance_offset(fmod, 1, lm)
            pares = cavity.anti_resonance_offset(fmod, 1, lm)
            prows.append([1, roc_offset, lm, pres, pares])
            pres = cavity.resonance_offset(fmod, 2, lm)
            pares = cavity.anti_resonance_offset(fmod, 2, lm)
            prows.append([2, roc_offset, lm, pres, pares])

            nres = cavity.resonance_offset(-fmod, -1, lm)
            nares = cavity.anti_resonance_offset(-fmod, -1, lm)
            nrows.append([1, roc_offset, lm, nres, nares])
            nres = cavity.resonance_offset(-fmod, -2, lm)
            nares = cavity.anti_resonance_offset(-fmod, -2, lm)
            nrows.append([2, roc_offset, lm, nres, nares])

    # Sort by resonance/anti-resonance.
    prows = sorted(prows, key=lambda r: min([r[3], r[4]]))
    nrows = sorted(nrows, key=lambda r: min([r[3], r[4]]))

    header = ["FSR offset", "RoC offset", "l+m", "offset from resonance",
              "offset from anti-resonance"]

    print(f"Upper low frequency sideband (f1 = {round(fmod / 1e6, 2)} MHz)")
    print(tabulate(prows[:nresults], header))
    print()
    print(f"Lower low frequency sideband (f1 = {round(-fmod / 1e6, 2)} MHz)")
    print(tabulate(nrows[:nresults], header))