"""Example cavity information dump using the
AEI 10 m prototype SQL parameters."""

from gauss import FabryPerot


if __name__ == "__main__":
    cavity = FabryPerot(11.3673, 1 - 8220e-6, 1 - 16e-6, 5.7, 5.7)
    print(cavity)