"""Print table of clipping ratios of higher order modes on the AEI
10 m prototype optics.

Uses a numerical integrator with adaptive step size to calculate the
power clipped by apertures of varying size. The integration tolerance
must be set very low due to the wide regions of close to zero power
in the Hermite-Gauss function.

This simulation should be run on a server with many (>30) cores. If you
run this on a consumer grade processor, reduce nprocess to <= your number
of cores and consider reducing maxhom.
"""

from multiprocessing import Pool
import numpy as np
from scipy.integrate import dblquad
from tabulate import tabulate
from gauss.beam import HermiteGaussian, BeamParam


if __name__ == "__main__":
    maxhom = 30
    nprocess = 31 # +1 because 0 is also simulated

    # Integration settings.
    settings = {"epsabs": 1e-25, "epsrel": 1e-25}

    # Beam at mirror.
    q = BeamParam(w=6e-3, rc=11.3673/2)

    radii = [100e-3, 50e-3, 25e-3, 24e-3, 23e-3, 22e-3, 21e-3, 20e-3, 19e-3, 18e-3, 17e-3]
    header = ["l", "full"]

    for radius in radii[1:]:
        header.extend([f"{radius * 1000} mm", "fraction"])

    table = []
    homs = range(maxhom + 1)

    def row(hom):
        print(f"Order {hom}/{maxhom}")
        beam = HermiteGaussian(q, n=hom)
        # Unclipped beam power
        full_power, __ = beam.integrate_circle_power(radii[0], **settings)
        row = [hom, full_power]
        for radius in radii[1:]:
            power, __ = beam.integrate_circle_power(radius, **settings)
            fraction = power / full_power
            row.extend([power, fraction])
        print(f"Finished {hom}")
        return row
    
    # Create pool of workers.
    with Pool(nprocess) as pool:
        table = pool.map(row, homs)

    print(tabulate(table, header))