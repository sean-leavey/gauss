"""Reproduce table of sideband offsets from T070247"""

import numpy as np
from tabulate import tabulate

from gauss.cavity import FabryPerot


if __name__ == "__main__":
    # Cavity and sideband parameters; note these are out of date but match those
    # of T070247.
    lcav = 3994.75
    itm_t_p = 0.014
    etm_t_p = 5e-6
    itm_roc = 1971
    etm_roc = 2191
    fmod1 = 9.09627e6
    fmod2 = 45.48135e6

    cavity = FabryPerot(lcav, 1 - itm_t_p, 1 - etm_t_p, itm_roc, etm_roc)
    print(cavity)
    print()

    def compute_row(cavity, fmod, n, lm):
        return [n, lm, cavity.resonance_offset(fmod, n, lm),
                cavity.anti_resonance_offset(fmod, n, lm)]

    header = ["FSR offset", "l+m", "offset from resonance",
              "offset from anti-resonance"]

    print(f"Upper low frequency sideband (f1 = {round(fmod1 / 1e6, 2)} MHz)")
    rows = []
    rows.append(compute_row(cavity, fmod1, 242, 0))
    rows.append(compute_row(cavity, fmod1, 242, 1))
    rows.append(compute_row(cavity, fmod1, 241, 1))
    rows.append(compute_row(cavity, fmod1, 241, 2))
    rows.append(compute_row(cavity, fmod1, 240, 2))
    rows.append(compute_row(cavity, fmod1, 240, 3))
    rows.append(compute_row(cavity, fmod1, 239, 4))
    rows.append(compute_row(cavity, fmod1, 238, 5))
    rows.append(compute_row(cavity, fmod1, 237, 6))
    print(tabulate(rows, header))
    print()
    print(f"Lower low frequency sideband (f1 = {round(-fmod1 / 1e6, 2)} MHz)")
    rows = []
    rows.append(compute_row(cavity, -fmod1, -242, 0))
    rows.append(compute_row(cavity, -fmod1, -243, 0))
    rows.append(compute_row(cavity, -fmod1, -243, 1))
    rows.append(compute_row(cavity, -fmod1, -244, 2))
    rows.append(compute_row(cavity, -fmod1, -245, 3))
    rows.append(compute_row(cavity, -fmod1, -246, 4))
    rows.append(compute_row(cavity, -fmod1, -247, 5))
    rows.append(compute_row(cavity, -fmod1, -248, 6))
    print(tabulate(rows, header))
    print()
    print(f"Upper high frequency sideband (f2 = {round(fmod2 / 1e6, 2)} MHz)")
    rows = []
    rows.append(compute_row(cavity, fmod2, 1212, 0))
    rows.append(compute_row(cavity, fmod2, 1211, 1))
    rows.append(compute_row(cavity, fmod2, 1210, 2))
    rows.append(compute_row(cavity, fmod2, 1209, 3))
    rows.append(compute_row(cavity, fmod2, 1209, 4))
    rows.append(compute_row(cavity, fmod2, 1208, 4))
    rows.append(compute_row(cavity, fmod2, 1208, 5))
    rows.append(compute_row(cavity, fmod2, 1207, 6))
    print(tabulate(rows, header))
    print()
    print(f"Lower high frequency sideband (f2 = {round(-fmod2 / 1e6, 2)} MHz)")
    rows = []
    rows.append(compute_row(cavity, -fmod2, -1212, 0))
    rows.append(compute_row(cavity, -fmod2, -1213, 1))
    rows.append(compute_row(cavity, -fmod2, -1214, 2))
    rows.append(compute_row(cavity, -fmod2, -1215, 3))
    rows.append(compute_row(cavity, -fmod2, -1216, 4))
    rows.append(compute_row(cavity, -fmod2, -1216, 5))
    rows.append(compute_row(cavity, -fmod2, -1217, 5))
    rows.append(compute_row(cavity, -fmod2, -1217, 6))
    rows.append(compute_row(cavity, -fmod2, -1218, 7))
    print(tabulate(rows, header))
    print()