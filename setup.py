from setuptools import setup, find_packages

REQUIREMENTS = [
    "numpy",
    "scipy",
    "sympy",
    "matplotlib",
    "tabulate",
]

setup(
    name="gauss",
    version="0.2.0",
    description="Gaussian optics",
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    license="GPLv3",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7"
    ]
)
