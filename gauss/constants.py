"""Constants for AEI 10 m prototype context."""

# Namespace some SciPy constants (https://docs.scipy.org/doc/scipy/reference/constants.html).
from scipy.constants import speed_of_light as c0, Boltzmann as kb

# Default laser wavelength.
lambda0 = 1064e-9