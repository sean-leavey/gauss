Gauss
-----

This is a library of tools for use in calculating cavity optical properties.

Sean Leavey  
<sean.leavey@ligo.org>