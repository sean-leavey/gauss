"""Beam properties.

Some code adapted from Pykat <https://git.ligo.org/finesse/pykat>.
"""

import math
from copy import deepcopy
import numpy as np
from scipy.special import hermite
from scipy.integrate import dblquad
import matplotlib.pyplot as plt

from .constants import c0, lambda0


class BeamParam:
    """Gaussian beam complex parameter.
    
    BeamParam is effectively a complex number with extra
    functionality to determine beam parameters.
    
    Defaults to 1064e-9m for wavelength and refractive index 1.

    Usage:
        q = BeamParam(w0=w0, z=z)
        q = BeamParam(z=z, zr=zr)
        q = BeamParam(w=w, rc=rc)
        q = BeamParam(q=a) # where a is a complex number
    """
    def __init__(self, wavelength=1064e-9, nr=1, **kwargs):
        self._q = None
        self._lambda0 = wavelength
        self._nr = nr
        
        bperror = ValueError("Must specify: z and w0 or z and zr or rc and w or q, to define the beam parameter")

        if len(kwargs) == 1:
            if "q" in kwargs:
                self._q = complex(kwargs["q"])        
            else:
                raise bperror
        elif len(kwargs) == 2:
            if "w0" in kwargs and "z" in kwargs:
                q = float(kwargs["z"]) + 1j * math.pi * float(kwargs["w0"]) ** 2 / (self.wavelength / self._nr)
            elif "z" in kwargs and "zr" in kwargs:
                q = float(kwargs["z"]) + 1j * float(kwargs["zr"])
            elif "rc" in kwargs and "w" in kwargs:
                one_q = 1 / float(kwargs["rc"]) - 1j * float(wavelength) / (math.pi * float(nr) * float(kwargs["w"]) ** 2)
                q = 1 / one_q
            else:
                raise bperror
                
            self._q = q
        else:
            raise ValueError("Too many parameters supplied")
    
    @property
    def wavelength(self):
        return self._lambda0

    @wavelength.setter
    def wavelength(self, value):
        self._lambda0 = float(value)
        
    @property
    def nr(self):
        return self._nr
    
    @property
    def q(self):
        return self._q
    
    @property
    def z(self):
        return self._q.real
    
    @z.setter
    def z(self, value):
        self._q = complex(1j * self.q.imag + float(value))
    
    @property
    def zr(self):
        return self._q.imag

    @zr.setter
    def zr(self, value):
        self._q = complex(self.q.real + 1j * float(value))
        
    @property
    def w(self, z=None):
        return np.abs(self.q)* np.sqrt(self.wavelength / (self.nr * math.pi * self.q.imag))
    
    def beamsize(self, z=None, wavelength=None, nr=None, w0=None):
        if z is None:
            z = self.z
        else:
            z = np.array(z + self.z)
                
        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)
            
        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)
            
        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)
        
        q = z + 1j * math.pi * w0 **2 / (wavelength/nr)
        
        return np.abs(q)*np.sqrt(wavelength / (nr * math.pi * q.imag))
    
    def gouy(self, z=None, wavelength=None, nr=None, w0=None):
        if z is None:
            z = self.z
        else:
            z = np.array(z + self.z)
                
        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)
            
        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)
            
        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)
        
        q = z + 1j * math.pi * w0 **2 / (wavelength*nr)
        
        return np.arctan2(q.real, q.imag)
        
    @property
    def divergence(self):
        return self.wavelength / (self.w0 * np.pi)
        
    @property
    def w0(self):
        return np.sqrt(self.q.imag * self.wavelength / (self.nr * math.pi))    

    @w0.setter
    def w0(self,value ):
        self.__q = complex(self.q.real + 1j*value**2 * (self.nr * math.pi) / self.wavelength)

    @property
    def roc(self):
        def __rc(z, zr):
            if z != 0:
                return z * (1 + (zr / z) ** 2)
            else:
                return float("inf")
                
        v = np.vectorize(__rc)
        
        return v(self.z, self.zr)
    
    def curvature(self, z=None, wavelength=None, nr=None, w0=None):
        if z is None:
            z = self.z
        else:
            z = np.array(z)
                
        if wavelength is None:
            wavelength = self.wavelength
        else:
            wavelength = np.array(wavelength)
            
        if nr is None:
            nr = self.nr
        else:
            nr = np.array(nr)
            
        if w0 is None:
            w0 = self.w0
        else:
            w0 = np.array(w0)
        
        q = z + 1j * math.pi * w0 **2 / (wavelength*nr)
        
        return q.real * (1+ (q.imag/q.real)**2)        
        
    def conjugate(self):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q.conjugate())
    
    def __abs__(self):
        return abs(complex(self.q))
        
    def __complex__(self):
        return self.q
    
    def __str__(self):
        return str(self.q)
    
    def __mul__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q * complex(a))
    
    def __imul__(self, a):
        self.q *= complex(a)
        return self
        
    __rmul__ = __mul__
    
    def __add__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q + complex(a))
    
    def __iadd__(self, a):
        self.__q += complex(a)
        return self
        
    __radd__ = __add__
    
    def __sub__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q - complex(a))
    
    def __isub__(self, a):
        self.__q -= complex(a)
        return self
        
    def __rsub__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=complex(a) - self.q)
    
    def __div__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q / complex(a))
        
    def __truediv__(self, a):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q / complex(a))
    
    def __idiv__(self, a):
        self.__q /= complex(a)
        return self
    
    def __pow__(self, q):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=self.q ** q)

    def __neg__(self):
        return self.__class__(wavelength=self.wavelength, nr=self.nr, q=-self.q)
        
    def __eq__(self, q):
        if q is None:
            return False
            
        return complex(q) == self.q
        
    @property
    def real(self):
        return self.q.real

    @real.setter
    def real(self, value):
        self.q.real = float(value)
    
    @property
    def imag(self):
        return self.q.imag

    @imag.setter
    def imag(self, value):
        self.q.imag = float(value)

    def reverse(self):
        """Reverse beam direction."""
        self.q = -1.0 * self.q.real + 1j * self.q.imag


class HermiteGaussian:
    def __init__(self, qx, qy=None, n=0, m=0):
        if qy is None:
            qy = deepcopy(qx)
        self.qx = qx
        self.qy = qy
        self.n = n
        self.m = m
    
    @property
    def q(self):
        return (self.qx.q, self.qy.q)

    def _h(self, order):
        return hermite(order)

    @property
    def hn(self):
        return self._h(self.n)
    
    @property
    def hm(self):
        return self._h(self.m)

    def _coeff_q(self, q):
        const = math.pow(2.0 / math.pi, 0.25)
        const *= np.sqrt(1.0 / (q.w0 * 2 ** self.n * np.math.factorial(self.n)))
        const *= np.sqrt(1j * q.imag / q.q)
        const *= ((1j * q.imag * q.q.conjugate()) / (-1j * q.imag * q.q)) ** (self.n / 2.0)
        return const

    def _k(self, q):
        return 2 * math.pi / q.wavelength

    def _u(self, order, q, pos):
        sqrt2_wxz = math.sqrt(2) / q.w
        return self._coeff_q(q) * self._h(order)(sqrt2_wxz * pos) * np.exp(-0.5j * self._k(q) * pos * pos / q.q)

    def un(self, x):
        return self._u(self.n, self.qx, x)
    
    def um(self, y):
        return self._u(self.m, self.qy, y)
        
    def unm(self, x, y):
        return np.outer(self.un(x), self.um(y))

    def integrate_circle_power(self, radius, **kwargs):
        """Calculate power in a circle centred on 0 with the specified radius.
        
        Parameters
        ----------
        radius : float
            Integration area radius.

        Returns
        -------
        float
            Normalised power in the integrated area.
        float
            Integration error estimate (see SciPy dblquad docs).
        """
        xlimlow = -radius
        xlimhigh = radius

        def beam_power(y, x):
            field = self.unm(x, y)
            return np.squeeze(np.abs(field * field.conj()))

        def yhigh(x):
            """Generate y upper bound from x"""
            return np.sqrt(radius ** 2 - x ** 2)

        def ylow(x):
            """Generate y lower bound from x"""
            return -yhigh(x)

        return dblquad(beam_power, xlimlow, xlimhigh, ylow, yhigh, **kwargs)

    def plot(self, ndx=100, ndy=100, xscale=4, yscale=4):
        """ Make a simple plot of the mode """
        xpos = xscale * np.linspace(-self.qx.w, self.qx.w, ndx)
        ypos = yscale * np.linspace(-self.qy.w, self.qy.w, ndy)
        dx = xpos[1] - xpos[0]
        dy = ypos[1] - ypos[0]
        data = self.unm(xpos, ypos)

        fig = plt.figure()
        axes = plt.imshow(np.abs(data.T), aspect=dx/dy,
                          extent=[min(xpos), max(xpos),
                                  min(ypos), max(ypos)])
        plt.xlabel('x (m)')
        plt.ylabel('y (m)')
    
    def show(self):
        plt.show()