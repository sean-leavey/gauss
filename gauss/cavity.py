"""Cavity properties"""

import numpy as np
from .constants import c0, lambda0


class Cavity:
    pass


class FabryPerot(Cavity):
    def __init__(self, length, r1_p, r2_p, rc1, rc2, lambda0=lambda0):
        super().__init__()
        
        self.length = length
        self.r1_p = r1_p
        self.r2_p = r2_p
        self.rc1 = rc1
        self.rc2 = rc2
        self.lambda0 = lambda0
    
    @property
    def r1_a(self):
        return np.sqrt(self.r1_p)
    
    @property
    def r2_a(self):
        return np.sqrt(self.r2_p)

    @property
    def fsr(self):
        return c0 / (2 * self.length)

    @property
    def fwhm(self):
        """Cavity full-width at half-maximum"""
        return 2 * self.f_pole
    
    @property
    def f_pole(self):
        """Cavity pole frequency"""
        return self.fsr / np.pi * np.arcsin(
                    (1 - self.r1_a * self.r2_a) /
                    (2 * np.sqrt(self.r1_a * self.r2_a))
               )

    @property
    def finesse(self):
        return self.fsr / self.fwhm

    @property
    def g_factor(self):
        return self.g1 * self.g2

    @property
    def g1(self):
        return 1 - self.length / self.rc1

    @property
    def g2(self):
        return 1 - self.length / self.rc2

    @property
    def w0(self):
        return np.sqrt(self.zr * self.lambda0 / np.pi)
    
    @property
    def w1(self):
        return np.sqrt(self.length * self.lambda0 / np.pi
                       * np.sqrt(self.g2 / (self.g1 * (1 - self.g_factor))))
    
    @property
    def w2(self):
        return np.sqrt(self.length * self.lambda0 / np.pi
                       * np.sqrt(self.g1 / (self.g2 * (1 - self.g_factor))))

    @property
    def zr(self):
        return np.sqrt(self.length ** 2 * self.g_factor * (1 - self.g_factor)
                       / (self.g1 + self.g2 - 2 * self.g_factor) ** 2)
    
    @property
    def z1(self):
        return -self.length * self.g2 * (1 - self.g1) / (self.g1 + self.g2 - 2 * self.g_factor)

    @property
    def z2(self):
        return self.length * self.g1 * (1 - self.g2) / (self.g1 + self.g2 - 2 * self.g_factor)
    
    @property
    def gouy1(self):
        return np.arctan2(self.z1, self.zr)
    
    @property
    def gouy2(self):
        return np.arctan2(self.z2, self.zr)
    
    @property
    def gouyt(self):
        """Round-trip gouy phase, in radians"""
        gouyt = 2 * (self.gouy2 - self.gouy1)
    
        # Constrain gouy phase to +/-90 degrees
        if gouyt > np.pi:
            gouyt -= 2 * np.pi
        
        return gouyt
    
    @property
    def mode_separation(self):
        """Higher order mode separation, in Hz"""
        return self.fsr * self.gouyt / (2 * np.pi)
    
    @property
    def mode_separation_p(self):
        """Mode separation, guaranteed to be positive by wrapping an FSR if needbe"""
        msep = self.mode_separation
        if msep < 0:
            msep += self.fsr
        return msep

    def resonance_offset(self, frequency, n, lm=0):
        """Compute offset of specified frequency from the nth cavity resonance.
        
        The offset is returned in absolute form.
        
        Parameters
        ----------
        frequency : :class:`float`
            The frequency to check against resonance.
        n : :class:`float`
            The FSR offset.
        lm : :class:`int`, optional
            The l+m transverse mode order. Defaults to the fundamental mode.
        
        Returns
        -------
        :class:`float`
            Frequency offset, in absolute form.
        """
        lm = int(lm)
        offset = (n * self.fsr + lm * self.mode_separation_p) % frequency
        if abs(offset) > abs(offset - frequency):
            # The previous resonance is closer.
            offset = abs(offset - frequency)
        # Apply abs last because sign is required above.
        return abs(offset)
    
    def anti_resonance_offset(self, frequency, n, lm):
        """Compute offset of specified frequency from the nth cavity anti-resonance.
        
        See :meth:`.resonance_offset` for parameters.
        """
        return self.resonance_offset(frequency, n + 0.5, lm)

    def __str__(self):
        return f"""Fabry-Perot cavity
    Length: {self.length} m
    Beam waist size: {self.w0 * 1e6} μm
    FWHM: {self.fwhm * 1e-3} kHz
    Free spectral range: {self.fsr * 1e-6} MHz
    Finesse: {self.finesse}
    Round-trip gouy phase: {np.degrees(self.gouyt)}°
    Higher order mode separation: {self.mode_separation * 1e-3} kHz
    Mirror 1:
        Reflectivity (power): {self.r1_p}
        Radius of curvature: {self.rc1} m
        Beam size: {self.w1 * 1e3} mm
        Distance to beam waist: {self.z1} m
        G-factor: {self.g1}
        Gouy phase: {np.degrees(self.gouy1)}°
    Mirror 2:
        Reflectivity (power): {self.r2_p}
        Radius of curvature: {self.rc2} m
        Beam size: {self.w2 * 1e3} mm
        Distance to beam waist: {self.z2} m
        G-factor: {self.g2}
        Gouy phase: {np.degrees(self.gouy2)}°"""