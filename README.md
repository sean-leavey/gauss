# Gauss
Cavity optics calculator.

## Example
A Fabry-Perot cavity (an AEI 10 m prototype arm cavity) is shown in
the `examples` directory. It outputs the following text:

```
Fabry-Perot cavity
    Length: 11.3673 m
    Beam waist size: 321.31584947142966 μm
    FWHM: 17.356338128047433 kHz
    Free spectral range: 13.186616786747953 MHz
    Finesse: 759.7580024923974
    Round-trip gouy phase: -12.28038312952935°
    Higher order mode separation: -449.824184232078 kHz
    Mirror 1:
        Reflectivity (power): 0.99178
        Radius of curvature: 5.7 m
        Beam size: 5.999439770954381 mm
        Distance to beam waist: -5.68365 m
        G-factor: -0.9942631578947367
        Gouy phase: -86.92990421761766°
    Mirror 2:
        Reflectivity (power): 0.999984
        Radius of curvature: 5.7 m
        Beam size: 5.999439770954381 mm
        Distance to beam waist: 5.68365 m
        G-factor: -0.9942631578947367
        Gouy phase: 86.92990421761766°
```

## Tests
Unit tests are provided that check the results against Finesse. These
are in the `tests` directory and can be run with `python -m unittest [test_file.py]`.

## Credits
Sean Leavey  
<sean.leavey@ligo.org>